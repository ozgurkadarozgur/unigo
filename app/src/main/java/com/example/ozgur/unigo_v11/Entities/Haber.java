package com.example.ozgur.unigo_v11.Entities;

public class Haber {

    private int id;
    private HaberRendered title;
    private HaberRendered content;
    private HaberRendered excerpt;
    private int featured_media;
    private String picture_url;

    public Haber(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HaberRendered getTitle() {
        return title;
    }

    public void setTitle(HaberRendered title) {
        this.title = title;
    }

    public HaberRendered getContent() {
        return content;
    }

    public void setContent(HaberRendered content) {
        this.content = content;
    }

    public HaberRendered getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(HaberRendered excerpt) {
        this.excerpt = excerpt;
    }

    public int getFeatured_media() {
        return featured_media;
    }

    public void setFeatured_media(int featured_media) {
        this.featured_media = featured_media;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }
}
