package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.Entities.Konu;
import com.example.ozgur.unigo_v11.POJO.Subject;
import com.example.ozgur.unigo_v11.R;

import java.util.List;

public class KonuListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Subject> mKonuList;


    public KonuListAdapter(Activity activity, List<Subject> konuList){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mKonuList = konuList;
    }


    @Override
    public int getCount() {
        return mKonuList.size();
    }

    @Override
    public Object getItem(int position) {
        return mKonuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;

        rowView = mInflater.inflate(R.layout.konu_list_layout, null);


        TextView konu_text = rowView.findViewById(R.id.konu_text);
        CheckBox konu_check = rowView.findViewById(R.id.konu_check);
        konu_check.setTag(position);

        final Subject konu = mKonuList.get(position);

        konu_text.setText(konu.getSubject());
        konu_text.setTextColor(Color.BLACK);
        konu_check.setChecked(konu.getSelected());

        konu_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Subject item_konu = mKonuList.get(position);
                item_konu.setSelected(isChecked);
                /*
                if (isChecked) item_konu.setChecked(1);
                else item_konu.setChecked(0);
                */
                Log.d("button_tag", buttonView.getTag().toString());
            }
        });


        return rowView;
    }
}
