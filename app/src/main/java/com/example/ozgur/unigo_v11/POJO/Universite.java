package com.example.ozgur.unigo_v11.POJO;

public class Universite {

    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    "id":-1,
      "name":"Hedefinizdeki Üniversite",
      "slug":"abant-izzet-baysal-universitesi",
      "short_name":"IBÜ",
      "osym_id":"1001",
      "city_id":14,
      "university_type":"state_university",
      "campus_type":"campus",
      "location_type":"outside_city_location",
      "foundation":"",
      "founded_on":"1992-07-03",
      "phone":"0.374.254 10 00",
      "email":"bilgi@ibu.edu.tr",
      "website":"www.ibu.edu.tr",
      "address":"Abant İzzet Baysal Üniversitesi Gölköy Kampüsü 14280 BOLU",
      "latitude":40.904583,
      "longitude":31.179722
     */

}
