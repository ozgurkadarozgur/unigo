package com.example.ozgur.unigo_v11.Entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Sinav {

    private int id;

    private String sinav_adi;

    private String color_code;

    public Sinav(){

    }

    public Sinav(String sinav_adi){
        this.sinav_adi = sinav_adi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSinav_adi(String sinav_adi) {
        this.sinav_adi = sinav_adi;
    }

    public String getSinav_adi() {
        return sinav_adi;
    }

    public String getColor_code() {
        return color_code;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }

    @Override
    public String toString() {
        return this.sinav_adi;
    }

}
