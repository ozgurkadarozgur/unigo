package com.example.ozgur.unigo_v11.Entities;

import com.google.gson.annotations.SerializedName;

public class HaberSourceUrl {

    @SerializedName("source_url")
    private String source_url;

    public HaberSourceUrl()
    {

    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }
}
