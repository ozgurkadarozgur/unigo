package com.example.ozgur.unigo_v11.Entities;

import com.google.gson.annotations.SerializedName;

public class HaberColorMag {

    @SerializedName("colormag-featured-post-small")
    private HaberSourceUrl colormag_default_news;

    public HaberColorMag()
    {

    }

    public HaberSourceUrl getColormag_default_news() {
        return colormag_default_news;
    }

    public void setColormag_default_news(HaberSourceUrl colormag_default_news) {
        this.colormag_default_news = colormag_default_news;
    }
}
