package com.example.ozgur.unigo_v11.api;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import okhttp3.OkHttpClient;

public class GetAPIClient {

    public static OkHttpClient getClient()
    {
        OkHttpClient mClient=new OkHttpClient();
        try {
            mClient = new OkHttpClient.Builder()
                    .sslSocketFactory(new TLSSocketFactory())
                    .build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return mClient;
    }

}
