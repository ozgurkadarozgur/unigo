package com.example.ozgur.unigo_v11.Activities.Formul;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.ListAdapters.FormulImageListAdapter;
import com.example.ozgur.unigo_v11.R;

import java.util.ArrayList;
import java.util.List;

public class FormulResimActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formul_resim);

        Toolbar toolbar = findViewById(R.id.formul_resim_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView toolbar_title = toolbar.findViewById(R.id.formul_resim_toolbar_title);

        String konu_tag = getIntent().getStringExtra("konu_tag");

        List<String> image_url_list = new ArrayList<>();

        switch (konu_tag) {
            case "cember": {
                toolbar_title.setText("Çember");
                for (int i = 1; i < 16; i++) {
                    image_url_list.add("geometri/cember/cember%20(" + i + ").png");
                }
                break;
            }
            case "cokgenler":{
                toolbar_title.setText("Çokgen");
                for (int i = 1; i < 16; i++) {
                    image_url_list.add("geometri/cokgenler/cokgenler%20(" + i + ").png");
                }
                break;
            }
            case "ucgenler":{
                toolbar_title.setText("Üçgen");
                for (int i = 1; i < 16; i++) {
                    image_url_list.add("geometri/ucgenler/ucgen%20(" + i + ").png");
                }
                break;
            }

        }

        FormulImageListAdapter adapter = new FormulImageListAdapter(FormulResimActivity.this, image_url_list);
        ListView formul_resim_list = findViewById(R.id.formul_resim_list_view);
        formul_resim_list.setAdapter(adapter);
        System.out.println("url_list_size " + image_url_list.size());
    }
}
