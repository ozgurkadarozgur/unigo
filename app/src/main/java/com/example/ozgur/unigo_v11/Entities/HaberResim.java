package com.example.ozgur.unigo_v11.Entities;

import com.google.gson.annotations.SerializedName;

public class HaberResim {

    //private HaberRendered guid;

    //private HaberMediaSize media_details;

    private String source_url;

    public HaberResim(){

    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    /*
    public HaberMediaSize getMedia_details() {
        return media_details;
    }

    public void setMedia_details(HaberMediaSize media_details) {
        this.media_details = media_details;
    }
*/

    /*
    public HaberRendered getGuid() {
        return guid;
    }

    public void setGuid(HaberRendered guid) {
        this.guid = guid;
    }
    */
}
