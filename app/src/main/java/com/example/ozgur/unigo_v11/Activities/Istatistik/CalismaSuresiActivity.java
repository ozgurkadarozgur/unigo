package com.example.ozgur.unigo_v11.Activities.Istatistik;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.ozgur.unigo_v11.R;

public class CalismaSuresiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calisma_suresi);
//a
        final Toolbar toolbar = findViewById(R.id.calisma_suresi_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void c_s_grafik(View view)
    {
        Intent intent = new Intent(this, C_S_GrafikActivity.class);
        startActivity(intent);
    }

}
