package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.ozgur.unigo_v11.POJO.FormulImage;
import com.example.ozgur.unigo_v11.R;
import com.example.ozgur.unigo_v11.api.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FormulImageListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<String> mFormulList;

    public FormulImageListAdapter(Activity activity, List<String> formulImageList){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mFormulList = formulImageList;
    }

    @Override
    public int getCount() {
        return mFormulList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFormulList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;

        rowView = mInflater.inflate(R.layout.formul_resim_layout, null);

        ImageView imageView = rowView.findViewById(R.id.formul_image);

        final String image = mFormulList.get(position);

        Picasso.get().load(Utils.getFormulImageUrl() + image).into(imageView);

        return rowView;


    }
}
