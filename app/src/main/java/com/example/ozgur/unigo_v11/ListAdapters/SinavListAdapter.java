package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.example.ozgur.unigo_v11.Activities.Konu.DersActivity;
import com.example.ozgur.unigo_v11.Entities.Sinav;
import com.example.ozgur.unigo_v11.R;

import java.util.List;

public class SinavListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Sinav> mSinavList;


    public SinavListAdapter(Activity activity, List<Sinav> sinavList){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSinavList = sinavList;
    }

    @Override
    public int getCount() {
        return mSinavList.size();
    }

    @Override
    public Object getItem(int position) {
        return mSinavList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View rowView;

        rowView = mInflater.inflate(R.layout.standard_list_layout, null);

        Button sinavButton = rowView.findViewById(R.id.layout_button);

        final Sinav sinav = mSinavList.get(position);

        final String sinav_adi = sinav.getSinav_adi() + " KONULARI";
        sinavButton.setText(sinav_adi);

        sinavButton.setBackgroundColor(Color.parseColor(sinav.getColor_code()));
        sinavButton.setTextColor(Color.WHITE);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DersActivity.class);
                intent.putExtra("sinav_id", sinav.getId());
                intent.putExtra("sinav_text", sinav_adi);
                intent.putExtra("color_code", sinav.getColor_code());
                v.getContext().startActivity(intent);
            }
        });

        /*
        sinavButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DersActivity.class);
                intent.putExtra("sinav_id", sinav.getId());
                intent.putExtra("color_code", sinav.getColor_code());
                v.getContext().startActivity(intent);
            }
        });
*/
        return rowView;

    }
}
