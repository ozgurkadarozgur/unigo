package com.example.ozgur.unigo_v11.Fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ozgur.unigo_v11.Entities.Haber;
import com.example.ozgur.unigo_v11.Entities.HaberResim;
import com.example.ozgur.unigo_v11.ListAdapters.HaberListAdapter;
import com.example.ozgur.unigo_v11.R;
import com.example.ozgur.unigo_v11.api.GetAPIClient;
import com.example.ozgur.unigo_v11.api.HaberClient;
import com.example.ozgur.unigo_v11.api.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MotivasyonFragment extends Fragment{

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_motivasyon, container, false);

        final ListView mHaberListView = view.findViewById(R.id.motivasyon_list_view);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.getHaberBaseUrl())
                .client(GetAPIClient.getClient())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        HaberClient haberClient = retrofit.create(HaberClient.class);
        Call<List<Haber>> call = haberClient.get_motivasyon();

        call.enqueue(new Callback<List<Haber>>() {
            @Override
            public void onResponse(Call<List<Haber>> call, Response<List<Haber>> response) {
                List<Haber> haberList = response.body();
                //HaberListAdapter adapter = new HaberListAdapter(getActivity(), haberList);
                //mHaberListView.setAdapter(adapter);


                for (final Haber mHaber : haberList) {
                    Retrofit.Builder builder = new Retrofit.Builder()
                            .baseUrl(Utils.getHaberPictureBaseUrl())
                            .client(GetAPIClient.getClient())
                            .addConverterFactory(GsonConverterFactory.create());

                    Retrofit retrofit = builder.build();
                    HaberClient client = retrofit.create(HaberClient.class);
                    Call<HaberResim> resim_call = client.get_haber_resim(mHaber.getFeatured_media());
                    resim_call.enqueue(new Callback<HaberResim>() {
                        @Override
                        public void onResponse(Call<HaberResim> call, Response<HaberResim> response) {
                            HaberResim mHaberResim = response.body();
                            Log.d("resim_err_name", mHaber.getTitle().getRendered());
                            //Log.d("resim_err", mHaberResim.getMedia_details().getSizes().getColormag_default_news().getSource_url());
                            //mHaber.setPicture_url(mHaberResim.getMedia_details().getSizes().getColormag_default_news().getSource_url());
                            mHaber.setPicture_url(mHaberResim.getSource_url());
                        }

                        @Override
                        public void onFailure(Call<HaberResim> call, Throwable t) {
                            t.printStackTrace();
                            Toast.makeText(getContext(), "ERROR_get_haber_resim",Toast.LENGTH_LONG).show();
                        }
                    });
                }



                HaberListAdapter adapter = new HaberListAdapter(getActivity(), haberList);
                mHaberListView.setAdapter(adapter);

                Log.d("haber_log", haberList.get(0).getTitle().getRendered());
            }

            @Override
            public void onFailure(Call<List<Haber>> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getContext(), "ERROR_get_haber",Toast.LENGTH_LONG).show();
            }
        });


        return view;
    }
}
