package com.example.ozgur.unigo_v11.Activities.Konu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.example.ozgur.unigo_v11.Entities.Sinav;
import com.example.ozgur.unigo_v11.ListAdapters.DurumListAdapter;
import com.example.ozgur.unigo_v11.MainActivity;
import com.example.ozgur.unigo_v11.R;

import java.util.ArrayList;
import java.util.List;

public class DurumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_durum);

        List<Sinav> sinavList = new ArrayList<>();
        sinavList.add(new Sinav("TYT"));
        sinavList.add(new Sinav("MF"));
        sinavList.add(new Sinav("TM"));
        sinavList.add(new Sinav("TS"));
        sinavList.add(new Sinav("DİL"));

        ProgressBar progressBar = findViewById(R.id.pb);
        progressBar.setProgress(40);

        /*
        final GridView durumGridView = findViewById(R.id.durum_grid_view);
        DurumListAdapter adapter = new DurumListAdapter(DurumActivity.this, sinavList);
        durumGridView.setAdapter(adapter);
        */
    }
}
