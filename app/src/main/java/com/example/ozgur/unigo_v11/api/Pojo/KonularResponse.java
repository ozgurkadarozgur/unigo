package com.example.ozgur.unigo_v11.api.Pojo;

import com.example.ozgur.unigo_v11.Entities.Ders;
import com.example.ozgur.unigo_v11.Entities.Konu;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KonularResponse {

    @SerializedName("ders")
    public Ders ders;

    @SerializedName("konular")
    public List<Konu> konular;

}
