package com.example.ozgur.unigo_v11.api;

import com.example.ozgur.unigo_v11.Entities.Konu;
import com.example.ozgur.unigo_v11.api.Pojo.KonularResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface KonuClient {

    /*
    @GET("api/v1/ders-{ders_id}-konular")
    Call<List<Konu>> get_konular(@Path("ders_id") int ders_id);
*/

    @GET("api/v1/ders-{ders_id}-konular")
    Call<KonularResponse> get_konular(@Path("ders_id") int ders_id);


}
