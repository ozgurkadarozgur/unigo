package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.Activities.Haber.HaberDetayActivity;
import com.example.ozgur.unigo_v11.Activities.Konu.DersActivity;
import com.example.ozgur.unigo_v11.Entities.Haber;
import com.example.ozgur.unigo_v11.Entities.Sinav;
import com.example.ozgur.unigo_v11.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

public class HaberListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Haber> mHaberList;

    public HaberListAdapter(Context activity, List<Haber> haberList){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mHaberList = haberList;
    }

    @Override
    public int getCount() {
        return mHaberList.size();
    }

    @Override
    public Object getItem(int position) {
        return mHaberList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;

        rowView = mInflater.inflate(R.layout.haber_list_layout, null);
        final Haber haber = mHaberList.get(position);

        TextView haber_title = rowView.findViewById(R.id.haber_title);
        TextView haber_content = rowView.findViewById(R.id.haber_content);
        ImageView haber_resim = rowView.findViewById(R.id.haber_resim);
        Picasso.get().load(haber.getPicture_url()).into(haber_resim, new Callback() {
            @Override
            public void onSuccess() {
                Log.d("picasso_log","success");
            }

            @Override
            public void onError(Exception e) {
                Log.d("picasso_log","error");
            }
        });
        //Picasso.get().load("https://www.universitego.com/wp-content/uploads/2018/08/kurban-bayrami.jpg").into(haber_resim);
        //new DownLoadImageTask(haber_resim).execute(haber.getPicture_url());


        haber_title.setText(Html.fromHtml(haber.getTitle().getRendered()));
        haber_content.setText(Html.fromHtml(haber.getExcerpt().getRendered()));

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HaberDetayActivity.class);
                intent.putExtra("haber_id", haber.getId());
                v.getContext().startActivity(intent);
            }
        });

        return rowView;
    }


    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                InputStream is = new URL(urlOfImage).openStream();
                /*
                    decodeStream(InputStream is)
                        Decode an input stream into a bitmap.
                 */
                logo = BitmapFactory.decodeStream(is);
            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */
        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }

}
