package com.example.ozgur.unigo_v11.Activities.Haber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.Entities.Haber;
import com.example.ozgur.unigo_v11.Entities.HaberDetay;
import com.example.ozgur.unigo_v11.R;
import com.example.ozgur.unigo_v11.api.HaberClient;
import com.example.ozgur.unigo_v11.api.Utils;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HaberDetayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haber_detay);

        final WebView haber_detay_content = findViewById(R.id.haber_detay_content);
        final ImageView haber_detay_resim = findViewById(R.id.haber_detay_resim);
        int haber_id = getIntent().getIntExtra("haber_id", 0);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.getHaberBaseUrl())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        HaberClient haberClient = retrofit.create(HaberClient.class);
        Call<HaberDetay> call = haberClient.get_haber_detay(haber_id);

        call.enqueue(new Callback<HaberDetay>() {
            @Override
            public void onResponse(Call<HaberDetay> call, Response<HaberDetay> response) {
                HaberDetay mHaberDetay = response.body();
                String picture_url = Utils.getHaberPictureBaseUrl() + mHaberDetay.getFeatured_media();
                Picasso.get().load(picture_url).into(haber_detay_resim);
                Log.d("picture_url", picture_url);

                WebSettings webSettings = haber_detay_content.getSettings();
                webSettings.setMinimumFontSize(45);
                webSettings.setLoadWithOverviewMode(true);
                webSettings.setUseWideViewPort(true);
                webSettings.setBuiltInZoomControls(false);
                webSettings.setDisplayZoomControls(false);
                haber_detay_content.loadData(mHaberDetay.getContent().getRendered(), "text/html; charset=utf-8","UTF-8");

                //haber_detay_content.setHtml(mHaberDetay.getContent().getRendered(), new HtmlResImageGetter(haber_detay_content));
                //haber_detay_content.setText(Html.fromHtml(mHaberDetay.getContent().getRendered()));
            }

            @Override
            public void onFailure(Call<HaberDetay> call, Throwable t) {

            }
        });

    }
}
