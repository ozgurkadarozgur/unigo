package com.example.ozgur.unigo_v11.Activities.EdebiyatKutusu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.R;

public class RomanOzetDetayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roman_ozet_detay);

        Toolbar toolbar = (Toolbar) findViewById(R.id.roman_ozet_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String roman_baslik_text = getIntent().getStringExtra("roman_baslik");
        String roman_ozet_text = getIntent().getStringExtra("roman_ozet");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView roman_ozet_title = toolbar.findViewById(R.id.roman_ozet_toolbar_title);
        roman_ozet_title.setText(roman_baslik_text);

        TextView roman_baslik = findViewById(R.id.roman_baslik_text);
        TextView roman_ozet = findViewById(R.id.roman_ozet_text);

        roman_baslik.setText(roman_baslik_text);
        roman_ozet.setText(roman_ozet_text);
    }
}
