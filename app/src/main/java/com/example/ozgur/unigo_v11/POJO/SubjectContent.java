package com.example.ozgur.unigo_v11.POJO;

import java.util.List;

public class SubjectContent {

    private String color;
    private String name;
    private List<Subject> subjects;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
