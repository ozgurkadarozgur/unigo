package com.example.ozgur.unigo_v11.api;


import com.example.ozgur.unigo_v11.Entities.Haber;
import com.example.ozgur.unigo_v11.Entities.HaberDetay;
import com.example.ozgur.unigo_v11.Entities.HaberResim;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HaberClient {

    @GET("posts?categories=5")
    Call<List<Haber>> get_haberler();

    @GET("{media_id}")
    Call<HaberResim> get_haber_resim(@Path("media_id") int media_id);

    @GET("posts?categories=4")
    Call<List<Haber>> get_rehberlik();

    @GET("posts?categories=286")
    Call<List<Haber>> get_motivasyon();

    @GET("posts/{haber_id}")
    Call<HaberDetay> get_haber_detay(@Path("haber_id") int haber_id);


}
