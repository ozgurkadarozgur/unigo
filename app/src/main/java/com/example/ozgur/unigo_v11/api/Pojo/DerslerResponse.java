package com.example.ozgur.unigo_v11.api.Pojo;

import com.example.ozgur.unigo_v11.Entities.Ders;
import com.example.ozgur.unigo_v11.Entities.Sinav;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DerslerResponse {

    @SerializedName("sinav")
    public Sinav sinav;

    @SerializedName("dersler")
    public List<Ders> dersler;

}
