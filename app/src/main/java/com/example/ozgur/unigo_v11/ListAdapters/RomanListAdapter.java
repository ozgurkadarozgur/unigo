package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.Activities.EdebiyatKutusu.RomanOzetDetayActivity;
import com.example.ozgur.unigo_v11.POJO.RomanOzet;
import com.example.ozgur.unigo_v11.R;

import java.util.List;

public class RomanListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<RomanOzet> mRomanList;

    public RomanListAdapter(Activity activity, List<RomanOzet> romanList){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRomanList = romanList;
    }

    @Override
    public int getCount() {
        return mRomanList.size();
    }

    @Override
    public Object getItem(int position) {
        return mRomanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView;

        rowView = mInflater.inflate(R.layout.roman_list_layout, null);

        TextView roman_text = rowView.findViewById(R.id.roman_text);

        final RomanOzet roman = mRomanList.get(position);

        roman_text.setText(roman.getBaslik());

        roman_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RomanOzetDetayActivity.class);
                intent.putExtra("roman_baslik", roman.getBaslik());
                intent.putExtra("roman_ozet", roman.getOzet());
                v.getContext().startActivity(intent);
            }
        });


        return rowView;

    }
}
