package com.example.ozgur.unigo_v11.Activities.EdebiyatKutusu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.example.ozgur.unigo_v11.ListAdapters.RomanListAdapter;
import com.example.ozgur.unigo_v11.POJO.RomanOzet;
import com.example.ozgur.unigo_v11.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RomanOzetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roman_ozet);

        final Toolbar toolbar = findViewById(R.id.roman_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        List<RomanOzet> romanList = get_list();

        ListView roman_list_view = findViewById(R.id.roman_list_view);
        RomanListAdapter adapter = new RomanListAdapter(RomanOzetActivity.this, romanList);
        roman_list_view.setAdapter(adapter);

    }

    public List<RomanOzet> get_list() {

        List<RomanOzet> dataList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("ozet_liste");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                String baslik = jo_inside.getString("baslik");
                String ozet = jo_inside.getString("ozet");

                RomanOzet item = new RomanOzet();
                item.setBaslik(baslik);
                item.setOzet(ozet);
                dataList.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplication().getAssets().open("roman_ozet.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
