package com.example.ozgur.unigo_v11.POJO;

public class RomanOzet {

    private String baslik;
    private String ozet;

    public String getBaslik() {
        return baslik;
    }

    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }

    public String getOzet() {
        return ozet;
    }

    public void setOzet(String ozet) {
        this.ozet = ozet;
    }
}
