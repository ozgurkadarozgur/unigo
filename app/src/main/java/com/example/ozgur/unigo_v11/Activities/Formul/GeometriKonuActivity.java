package com.example.ozgur.unigo_v11.Activities.Formul;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.ozgur.unigo_v11.POJO.FormulImage;
import com.example.ozgur.unigo_v11.R;

public class GeometriKonuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geometri_konu);

        Toolbar toolbar = findViewById(R.id.geometri_konu_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void formul_image_view(View view)
    {
        Intent intent = new Intent(this, FormulResimActivity.class);
        String tag = view.getTag().toString();
        intent.putExtra("konu_tag", tag);
        startActivity(intent);
    }

}
