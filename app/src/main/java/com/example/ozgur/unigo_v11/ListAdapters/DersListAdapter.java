package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.ozgur.unigo_v11.Entities.Ders;
import com.example.ozgur.unigo_v11.Activities.Konu.KonuActivity;
import com.example.ozgur.unigo_v11.POJO.SubjectContent;
import com.example.ozgur.unigo_v11.R;

import java.util.List;

public class DersListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<SubjectContent> mDersList;
    private String mSinavTag;
    private Activity mActivity;

    public DersListAdapter(Activity activity, List<SubjectContent> dersList, String sinav_tag){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDersList = dersList;
        mSinavTag = sinav_tag;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mDersList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;

        rowView = mInflater.inflate(R.layout.standard_list_layout, null);

        Button dersButton = rowView.findViewById(R.id.layout_button);
        LinearLayout layout = rowView.findViewById(R.id.button_layout);


        final SubjectContent ders = mDersList.get(position);
        dersButton.setText(ders.getName());
        Drawable drawable = null;
        switch (mSinavTag){
            case "tyt":{
                drawable = mActivity.getResources().getDrawable(R.drawable.drawable_btn_tyt);
                break;
            }
            case "mf":{
                drawable = mActivity.getResources().getDrawable(R.drawable.drawable_btn_mf);
                break;
            }
            case "tm":{
                drawable = mActivity.getResources().getDrawable(R.drawable.drawable_btn_tm);
                break;
            }
            case "ts":{
                drawable = mActivity.getResources().getDrawable(R.drawable.drawable_btn_ts);
                break;
            }
            case "dil":{
                drawable = mActivity.getResources().getDrawable(R.drawable.drawable_btn_dil);
                break;
            }
        }

        layout.setBackground(drawable);
        //dersButton.setBackground(drawable);
        Log.d("m_sinav_tag", mSinavTag);
        dersButton.setTextColor(Color.WHITE);
        dersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), KonuActivity.class);
                intent.putExtra("ders_tag", ders.getName());
                intent.putExtra("sinav_tag", mSinavTag);
                v.getContext().startActivity(intent);

            }
        });

        return rowView;
    }
}
