package com.example.ozgur.unigo_v11.POJO;

public class FormulImage {

    private String image_url;

    public FormulImage(String image_url)
    {
        this.image_url = image_url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

}
