package com.example.ozgur.unigo_v11.api;

import android.support.v7.widget.LinearSmoothScroller;

import com.example.ozgur.unigo_v11.Entities.Ders;
import com.example.ozgur.unigo_v11.api.Pojo.DerslerResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DersClient {
/*
    @GET("api/v1/sinav-{sinav_id}-dersler")
    Call<List<Ders>> get_dersler(@Path("sinav_id") int sinav_id);
*/

    @GET("api/v1/sinav-{sinav_id}-dersler")
    Call<DerslerResponse> get_dersler(@Path("sinav_id") int sinav_id);

}
