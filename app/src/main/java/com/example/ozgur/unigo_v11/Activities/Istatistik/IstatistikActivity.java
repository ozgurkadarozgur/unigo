package com.example.ozgur.unigo_v11.Activities.Istatistik;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.ozgur.unigo_v11.R;

public class IstatistikActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_istatistik);

        final Toolbar toolbar = findViewById(R.id.istatistik_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


/*

        Button calisma_suresi_view_btn = findViewById(R.id.calisma_suresi_btn);
        calisma_suresi_view_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calisma_suresi_view(v);
            }
        });

        Button soru_sayisi_view_btn = findViewById(R.id.soru_sayisi_btn);
        soru_sayisi_view_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soru_sayisi_view(v);
            }
        });
 */

    }

    public void calisma_suresi_view(View view)
    {
        Intent intent = new Intent(this, CalismaSuresiActivity.class);
        startActivity(intent);
    }

    public void soru_sayisi_view(View view)
    {
        Intent intent = new Intent(this, SoruSayisiActivity.class);
        startActivity(intent);
    }

}
