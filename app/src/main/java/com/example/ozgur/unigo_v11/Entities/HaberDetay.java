package com.example.ozgur.unigo_v11.Entities;

public class HaberDetay {

    private HaberRendered title;
    private HaberRendered content;
    private int featured_media;

    public HaberRendered getTitle() {
        return title;
    }

    public void setTitle(HaberRendered title) {
        this.title = title;
    }

    public HaberRendered getContent() {
        return content;
    }

    public void setContent(HaberRendered content) {
        this.content = content;
    }

    public int getFeatured_media() {
        return featured_media;
    }

    public void setFeatured_media(int featured_media) {
        this.featured_media = featured_media;
    }
}
