package com.example.ozgur.unigo_v11.api;

import com.example.ozgur.unigo_v11.Entities.Sinav;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SinavClient {

    @GET("api/v1/get-sinavlar")
    Call<List<Sinav>> get_sinavlar ();

}
