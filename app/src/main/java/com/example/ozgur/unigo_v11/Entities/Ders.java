package com.example.ozgur.unigo_v11.Entities;

public class Ders {

    private int id;

    private String ders_adi;

    private int sinav_id;

    public Ders(){

    }

    public Ders(String ders_adi){
        this.ders_adi = ders_adi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDers_adi() {
        return ders_adi;
    }

    public void setDers_adi(String ders_adi) {
        this.ders_adi = ders_adi;
    }

    public int getSinav_id() {
        return sinav_id;
    }

    public void setSinav_id(int sinav_id) {
        this.sinav_id = sinav_id;
    }

    @Override
    public String toString() {
        return this.ders_adi;
    }

}
