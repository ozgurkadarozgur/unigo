package com.example.ozgur.unigo_v11.Entities;

public class Konu {

    private int id;

    private String konu_adi;

    private Ders ders;

    private int checked;

    public Konu(){}

    public Konu(String konu_adi){
        this.konu_adi = konu_adi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKonu_adi() {
        return konu_adi;
    }

    public void setKonu_adi(String konu_adi) {
        this.konu_adi = konu_adi;
    }

    public Ders getDers() {
        return ders;
    }

    public void setDers(Ders ders) {
        this.ders = ders;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public boolean is_checked()
    {
        int checked = this.checked;
        if(checked == 0) return false;
        else return true;
    }

    @Override
    public String toString() {
        return this.konu_adi;
    }
}
