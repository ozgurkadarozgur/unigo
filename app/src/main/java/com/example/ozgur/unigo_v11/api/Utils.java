package com.example.ozgur.unigo_v11.api;

public class Utils {

    private static final String BASE_URL = "http://unigo.prooys.com/unigo/public/";
    private static final String HABER_BASE_URL = "https://www.universitego.com/wp-json/wp/v2/";
    private static final String HABER_PICTURE_BASE_URL = "https://www.universitego.com/wp-json/wp/v2/media/";
    private static final String FORMUL_IMAGE_URL = "http://universitego.com/formuller/";

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getHaberBaseUrl() { return HABER_BASE_URL; }

    public static String getHaberPictureBaseUrl() { return HABER_PICTURE_BASE_URL; }

    public static String getFormulImageUrl() { return FORMUL_IMAGE_URL; }

}
