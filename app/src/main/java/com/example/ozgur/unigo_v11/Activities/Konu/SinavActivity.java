package com.example.ozgur.unigo_v11.Activities.Konu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.ozgur.unigo_v11.Entities.Sinav;
import com.example.ozgur.unigo_v11.ListAdapters.SinavListAdapter;
import com.example.ozgur.unigo_v11.POJO.SubjectItem;
import com.example.ozgur.unigo_v11.R;
import com.example.ozgur.unigo_v11.api.SinavClient;
import com.example.ozgur.unigo_v11.api.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SinavActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinav);

        Toolbar toolbar = findViewById(R.id.sinav_toolbar);
        setSupportActionBar(toolbar);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //final ListView mSinavList = findViewById(R.id.sinav_list_view);
        //get_list();
/*
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        SinavClient client = retrofit.create(SinavClient.class);
        Call<List<Sinav>> call = client.get_sinavlar();

        call.enqueue(new Callback<List<Sinav>>() {
            @Override
            public void onResponse(Call<List<Sinav>> call, Response<List<Sinav>> response) {
                List<Sinav> sinavList = response.body();
                SinavListAdapter adapter = new SinavListAdapter(SinavActivity.this, sinavList);
                mSinavList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Sinav>> call, Throwable t) {
                Toast.makeText(SinavActivity.this, "ERROR",Toast.LENGTH_LONG).show();
            }
        });
*/
    }

    public void dersler_view(View view)
    {
        Intent intent = new Intent(this, DersActivity.class);
        String sinav_tag = view.getTag().toString();
        intent.putExtra("sinav_tag", sinav_tag);
        startActivity(intent);
    }

    public void ilerleme_durum_view(View view)
    {
        Intent intent = new Intent(this, DurumActivity.class);
        startActivity(intent);
    }


}
