package com.example.ozgur.unigo_v11.Activities.Konu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ozgur.unigo_v11.Entities.Ders;
import com.example.ozgur.unigo_v11.ListAdapters.DersListAdapter;
import com.example.ozgur.unigo_v11.POJO.SubjectContent;
import com.example.ozgur.unigo_v11.POJO.SubjectItem;
import com.example.ozgur.unigo_v11.R;
import com.example.ozgur.unigo_v11.api.DersClient;
import com.example.ozgur.unigo_v11.api.Pojo.DerslerResponse;
import com.example.ozgur.unigo_v11.api.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ders);

        String sinav_tag = getIntent().getStringExtra("sinav_tag");

        //String ders_tag = getIntent().getStringExtra("ders_tag");

        final Toolbar toolbar = findViewById(R.id.ders_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ListView mDersList = findViewById(R.id.ders_list_view);
        final TextView toolbar_title = toolbar.findViewById(R.id.ders_toolbar_title);
        toolbar_title.setText(sinav_tag.toUpperCase());

        List<SubjectContent> dersList = get_list(sinav_tag);
        DersListAdapter adapter = new DersListAdapter(DersActivity.this, dersList, sinav_tag);
        mDersList.setAdapter(adapter);

/*

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        DersClient client = retrofit.create(DersClient.class);

        Call<DerslerResponse> call = client.get_dersler(sinav_id);
        call.enqueue(new Callback<DerslerResponse>() {
            @Override
            public void onResponse(Call<DerslerResponse> call, Response<DerslerResponse> response) {
                DerslerResponse derslerResponse = response.body();
                toolbar.setTitle(derslerResponse.sinav.getSinav_adi());
                DersListAdapter adapter = new DersListAdapter(DersActivity.this, derslerResponse.dersler, color_code);
                mDersList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<DerslerResponse> call, Throwable t) {
                Toast.makeText(DersActivity.this, "ERROR",Toast.LENGTH_LONG).show();
            }
        });

 */


/*
        DersClient client = retrofit.create(DersClient.class);


        Call<List<Ders>> call = client.get_dersler(sinav_id);

        call.enqueue(new Callback<List<Ders>>() {
            @Override
            public void onResponse(Call<List<Ders>> call, Response<List<Ders>> response) {
                List<Ders> dersList = response.body();
                DersListAdapter adapter = new DersListAdapter(DersActivity.this, dersList);
                mDersList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Ders>> call, Throwable t) {
                Toast.makeText(DersActivity.this, "ERROR",Toast.LENGTH_LONG).show();
            }
        });
*/

    }

    public List<SubjectContent> get_list(String tag) {

        List<SubjectContent> dataList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray(tag);

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                String subject_name = jo_inside.getString("name");
                String subject_color = jo_inside.getString("color");

                SubjectContent item = new SubjectContent();
                item.setName(subject_name);
                item.setColor(subject_color);
                dataList.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplication().getAssets().open("subjects.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
