package com.example.ozgur.unigo_v11;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.ozgur.unigo_v11.Activities.CalismaProgrami.CalismaProgramiActivity;
import com.example.ozgur.unigo_v11.Activities.CikmisSorular.CikmisSorularActivity;

import com.example.ozgur.unigo_v11.Activities.EdebiyatKutusu.EserSanatciActivity;
import com.example.ozgur.unigo_v11.Activities.EdebiyatKutusu.RomanOzetActivity;
import com.example.ozgur.unigo_v11.Activities.Formul.FizikKonuActivity;
import com.example.ozgur.unigo_v11.Activities.Formul.GeometriKonuActivity;
import com.example.ozgur.unigo_v11.Activities.GeriSayim.GeriSayimActivity;
import com.example.ozgur.unigo_v11.Activities.Istatistik.IstatistikActivity;
import com.example.ozgur.unigo_v11.Activities.Konu.SinavActivity;
import com.example.ozgur.unigo_v11.Activities.PuanHesap.PuanHesapActivity;
import com.example.ozgur.unigo_v11.Fragment.EdebiyatKutusuFragment;
import com.example.ozgur.unigo_v11.Fragment.FormullerFragment;
import com.example.ozgur.unigo_v11.Fragment.HaberFragment;
import com.example.ozgur.unigo_v11.Fragment.HakkimizdaFragment;
import com.example.ozgur.unigo_v11.Fragment.HedefBolumDialogFragment;
import com.example.ozgur.unigo_v11.Fragment.HomeFragment;
import com.example.ozgur.unigo_v11.Fragment.MotivasyonFragment;
import com.example.ozgur.unigo_v11.Fragment.RehberlikFragment;
import com.example.ozgur.unigo_v11.Fragment.UniversiteDialogFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (Build.VERSION.SDK_INT > 18){
            navigationView.setPadding(0, 50, 0 ,0);
        }

        loadFragment(new HomeFragment());

        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        //bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()){
                    case R.id.bottom_nav_home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.bottom_nav_haber:
                        fragment = new HaberFragment();
                        break;
                    case R.id.bottom_nav_rehberlik:
                        fragment = new RehberlikFragment();
                        break;
                    case R.id.bottom_nav_motivasyon:
                        fragment = new MotivasyonFragment();
                        break;

                }
                return loadFragment(fragment);
            }
        });




    }

    private boolean loadFragment(Fragment fragment)
    {
        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    public void uni_fragment(View view)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        UniversiteDialogFragment newFragment = new UniversiteDialogFragment();
        newFragment.show(fragmentManager, "dialog");
    }

    public void hedef_bolum_fragment(View view)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HedefBolumDialogFragment newFragment = new HedefBolumDialogFragment();
        newFragment.show(fragmentManager, "dialog");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        switch (id){

            case R.id.nav_anasayfa:{
                fragment = new HomeFragment();
                this.bottomNavigationView.setVisibility(View.VISIBLE);
                break;
            }

            case R.id.nav_hakkimizda:{
                fragment = new HakkimizdaFragment();
                this.bottomNavigationView.setVisibility(View.GONE);
                break;
            }

            case R.id.nav_edebiyat_kutusu:{
                this.bottomNavigationView.setVisibility(View.GONE);
                fragment = new EdebiyatKutusuFragment();
                break;
            }

            case R.id.nav_formuller:{
                this.bottomNavigationView.setVisibility(View.GONE);
                fragment = new FormullerFragment();
                break;
            }
        }

        loadFragment(fragment);

/*

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
 */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void geometri_konu_view(View view)
    {
        Intent intent = new Intent(this, GeometriKonuActivity.class);
        startActivity(intent);
    }

    public void fizik_konu_view(View view)
    {
        Intent intent = new Intent(this, FizikKonuActivity.class);
        startActivity(intent);
    }

    public void eser_sanatci_view(View view)
    {
        Intent intent = new Intent(this, EserSanatciActivity.class);
        startActivity(intent);
    }

    public void roman_ozet_view(View view)
    {
        Intent intent = new Intent(this, RomanOzetActivity.class);
        startActivity(intent);
    }


    public void sinavlar_view(View view)
    {
        Intent intent = new Intent(this, SinavActivity.class);
        startActivity(intent);
    }

    public void istatistikler_view(View view)
    {
        Intent intent = new Intent(this, IstatistikActivity.class);
        startActivity(intent);
    }

    public void puan_hesap_view(View view)
    {
        Intent intent = new Intent(this, PuanHesapActivity.class);
        startActivity(intent);
    }

    public void geri_sayim_view(View view)
    {
        Intent intent = new Intent(this, GeriSayimActivity.class);
        startActivity(intent);
    }

    public void cikmis_sorular_view(View view)
    {
        Intent intent = new Intent(this, CikmisSorularActivity.class);
        startActivity(intent);
    }

    public void calisma_programi_view(View view)
    {
        Intent intent = new Intent(this, CalismaProgramiActivity.class);
        startActivity(intent);
    }

}
