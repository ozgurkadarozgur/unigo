package com.example.ozgur.unigo_v11.POJO;

public class Subject {

    private String subject;
    private Boolean isSelected;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}
