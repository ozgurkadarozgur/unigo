package com.example.ozgur.unigo_v11.ListAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.Entities.Sinav;
import com.example.ozgur.unigo_v11.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class DurumListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Sinav> mSinavList;

    public DurumListAdapter(Activity activity, List<Sinav> sinavList){
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSinavList = sinavList;
    }
    @Override
    public int getCount() {
        return mSinavList.size();
    }

    @Override
    public Object getItem(int position) {
        return mSinavList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        ArrayList<PieEntry> mPieEntryList = new ArrayList<>();

        rowView = mInflater.inflate(R.layout.durum_list_layout, null);

        PieChart pieChart = rowView.findViewById(R.id.pie_chart);
        TextView pieText = rowView.findViewById(R.id.pie_title);

        Sinav sinav = mSinavList.get(position);

        pieText.setText(sinav.getSinav_adi());
        mPieEntryList.add(new PieEntry(40));
        mPieEntryList.add(new PieEntry(60));
        initialize_pie_chart(pieChart, mPieEntryList);

        return rowView;
    }

    private PieChart initialize_pie_chart(PieChart pieChart, ArrayList<PieEntry> pieEntryList)
    {

        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setCenterText("tamamlandi");
        pieChart.setHoleRadius(90);

        pieChart.setDrawSlicesUnderHole(false);
        ArrayList<PieEntry> yValues = pieEntryList;

        pieChart.animateY(1000, Easing.EasingOption.EaseInCubic);

        PieDataSet dataSet = new PieDataSet(yValues, "Countries");

        dataSet.setSliceSpace(0);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData data = new PieData(dataSet);
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.BLACK);
        pieChart.setData(data);

        return pieChart;
    }

}
