package com.example.ozgur.unigo_v11.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.ozgur.unigo_v11.POJO.EserSanatci;
import com.example.ozgur.unigo_v11.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class EserSanatciFragment extends Fragment {

    private TableLayout table;
    private EditText eser_search_text;
    List<EserSanatci> dataList;
    private ImageView close_img;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_eser_sanatci_layout, container, false);
        table = view.findViewById(R.id.tab_eser_sanatci_layout);
        eser_search_text = view.findViewById(R.id.eser_search_text);
        close_img = view.findViewById(R.id.eser_sanatci_close_icon);

        create_initial_list();

        close_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eser_search_text.setText(null);
            }
        });

        eser_search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (count > 0) close_img.setVisibility(View.VISIBLE);
                else close_img.setVisibility(View.INVISIBLE);

                fill_table(search_in_table(s.toString()));
                //search_in_table(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("deneme.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void create_initial_list() {

        dataList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("data");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                String yazar_value = jo_inside.getString("yazar");
                String eser_value = jo_inside.getString("eser");

                EserSanatci mObj = new EserSanatci();
                mObj.eser = eser_value;
                mObj.sanatci = yazar_value;
                dataList.add(mObj);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        fill_table(dataList);
    }

    public List<EserSanatci> search_in_table(String keyword) {

        List<EserSanatci> newList = new ArrayList<>();
        for (EserSanatci item :
                dataList) {
            if (item.eser.toLowerCase().contains(keyword.toLowerCase())){
                EserSanatci mObj = new EserSanatci();
                mObj.eser = item.eser;
                mObj.sanatci = item.sanatci;
                newList.add(mObj);
                Log.d("eser_found", item.eser);
            }
        }

        return newList;
        /*
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());

            for (int i = 0; i < dataList.size(); i++) {

                String yazar_value = dataList.get(i).get("");
                String eser_value = dataList.get(i).get("");

                TableRow row = new TableRow(getContext());
                row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1));

                TextView eserText = new TextView(getContext());
                eserText.setText(eser_value);
                TextView yazarText = new TextView(getContext());
                yazarText.setText(yazar_value);

                row.addView(eserText);
                row.addView(yazarText);
                table.addView(row, TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
         */
    }

    public void fill_table(List<EserSanatci> newList){
        clear_table();
        for (EserSanatci item :
                newList) {

            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1));

            TextView eserText = new TextView(getContext());
            eserText.setText(item.eser);
            TextView yazarText = new TextView(getContext());
            yazarText.setText(item.sanatci);

            eserText.setTextColor(Color.BLACK);
            yazarText.setTextColor(Color.BLACK);
            yazarText.setGravity(Gravity.END);
            row.addView(eserText);
            row.addView(yazarText);

            table.addView(row, TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
        }
    }

    public void clear_table(){
        //dataList.clear();
        table.removeAllViews();
    }
}
