package com.example.ozgur.unigo_v11.Activities.Konu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ozgur.unigo_v11.Entities.Konu;
import com.example.ozgur.unigo_v11.ListAdapters.KonuListAdapter;
import com.example.ozgur.unigo_v11.POJO.Subject;
import com.example.ozgur.unigo_v11.R;
import com.example.ozgur.unigo_v11.api.KonuClient;
import com.example.ozgur.unigo_v11.api.Pojo.KonularResponse;
import com.example.ozgur.unigo_v11.api.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KonuActivity extends AppCompatActivity {

    private Button mKaydetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konu);

        final Toolbar toolbar = findViewById(R.id.konu_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView toolbar_title = toolbar.findViewById(R.id.konu_toolbar_title);

        final String ders_tag = getIntent().getStringExtra("ders_tag");
        final String sinav_tag = getIntent().getStringExtra("sinav_tag");

        toolbar_title.setText(ders_tag);

        final ListView mKonuList = findViewById(R.id.konu_list_view);
        mKaydetButton = findViewById(R.id.konu_kaydet_button);

        //get_list(sinav_tag, ders_tag);

        List<Subject> konuList = get_list(sinav_tag, ders_tag);
        KonuListAdapter adapter = new KonuListAdapter(KonuActivity.this, konuList);
        mKonuList.setAdapter(adapter);

        mKaydetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_subject(ders_tag, sinav_tag, "Bölme ve Bölünebilme");
            }
        });

        /*
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        KonuClient client = retrofit.create(KonuClient.class);

        Call<KonularResponse> call = client.get_konular(ders_id);
        call.enqueue(new Callback<KonularResponse>() {
            @Override
            public void onResponse(Call<KonularResponse> call, Response<KonularResponse> response) {
                KonularResponse konularResponse = response.body();
                toolbar.setTitle(konularResponse.ders.getDers_adi());
                KonuListAdapter adapter = new KonuListAdapter(KonuActivity.this, konularResponse.konular);
                mKonuList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<KonularResponse> call, Throwable t) {
                Toast.makeText(KonuActivity.this, "ERROR",Toast.LENGTH_LONG).show();
            }
        });

         */

        /*
        Call<List<Konu>> call = client.get_konular(ders_id);

        call.enqueue(new Callback<List<Konu>>() {
            @Override
            public void onResponse(Call<List<Konu>> call, Response<List<Konu>> response) {
                List<Konu> konuList = response.body();
                KonuListAdapter adapter = new KonuListAdapter(KonuActivity.this, konuList);
                mKonuList.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Konu>> call, Throwable t) {
                Toast.makeText(KonuActivity.this, "ERROR",Toast.LENGTH_LONG).show();
            }
        });
         */


    }

    public void update_subject(String ders_tag, String sinav_tag, String konu_tag) {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray(sinav_tag);
            boolean check = false;
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                String m_ders_tag = jo_inside.getString("name");

                if (ders_tag.equals(m_ders_tag)) {
                    JSONArray array = jo_inside.getJSONArray("subjects");
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject object = array.getJSONObject(j);
                        String subject_value = object.getString("subject");
                        if (subject_value.equals(konu_tag)) {
                            object.put("isSelected", true);
                            check = true;
                            break;
                        }
                    }
                    if (check) break;
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Subject> get_list(String sinav_tag, String ders_tag) {

        List<Subject> dataList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray(sinav_tag);

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                String subject_name = jo_inside.getString("name");

                //Log.d("subject_name_log",subject_name);
                //Log.d("ders_name_log",ders_tag);


                if (subject_name.equals(ders_tag)) {
                    JSONArray subjectJSONArray = jo_inside.getJSONArray("subjects");
                    Type listType = new TypeToken<List<Subject>>() {
                    }.getType();
                    dataList = new Gson().fromJson(subjectJSONArray.toString(), listType);
                    break;
                }


                /*Boolean subject_selected = jo_inside.getBoolean("color");

                Subject item = new Subject();
                item.setSubject(subject_name);
                item.setSelected(subject_selected);
                dataList.add(item);
                */
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplication().getAssets().open("subjects.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
